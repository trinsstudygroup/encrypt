/*
	Name: Chris Hurst
	Class: CSC160
	Due Date: 09/12/15
	Files: encrypt.cpp, secret.dat
	Description: Program that prompts a user to enter a message to be encrypted using the ROT encryption method.
                 Set between 8-15 offset, and sends the encrypted message to secret.dat.
*/
#include <iostream>
#include <cstring>
#include <fstream>
#include <ctime>
#include <stdlib.h>

using namespace std;

int main() {

    int size, encrypt, n=8, m=15, spaces[10], numspaces=0;
    string msg, enmsg = "";

    srand(time(0));
    encrypt = rand() % (m - n + 1) + n;
    cout << "Please enter a message to encrypt: ";
    getline(cin, msg);

    size = msg.length();

    for(int i = 0; i < size; i++) {

        msg[i] = tolower(msg[i]);
// Start for the letters
        if (msg[i] >= 97 && msg[i] <= 122){
            if (msg[i] + encrypt > 122){
                enmsg += char((msg[i] + encrypt) - 26);
            }
            else {
                enmsg += char(msg[i] + encrypt);
            }
        }
// Start for the numbers
        else if (msg[i] >= 48 && msg[i] <= 57) {
            if (msg[i] + encrypt > 57) {
                if ((msg[i] + encrypt) - 10 > 57) {
                    enmsg += char((msg[i] + encrypt) - 20);
                }
                else {
                    enmsg += char((msg[i] + encrypt) - 10);
                }
            }

        }
// Start for spaces
        else if (msg[i] == 32) {
            spaces[numspaces] = i;
            numspaces++;

            if (msg[i - 1] >= 97 && msg[i - 1] <= 122) {
                if (msg[i - 1] + 4 > 122) {
                    enmsg += char((msg[i - 1] + 4) - 26);
                }
                else {
                    enmsg += char(msg[i - 1] + 4);
                }
            }
            else if (msg[i-1] >= 48 && msg[i-1] <= 57) {
                if (msg[i-1] + 4 > 57) {
                    if ((msg[i-1] + 4) - 10 > 57) {
                        enmsg += char((msg[i-1] + 4) - 20);
                    }
                    else {
                        enmsg += char((msg[i-1] + 4) - 10);
                    }
                }
            }
        }
    }
    enmsg += '&';

    ofstream outdata;
    outdata.open("secret.dat");

    outdata << encrypt << enmsg;

    for (int i = 0; i < numspaces; i++) {
        outdata << spaces[i] + 1;
        if (i + 1 < numspaces) {
            outdata << "#";
        }
    }
    cout << "\nThe message has been encrypted and transmitted" << endl;
    outdata.close();

    return 0;
}


